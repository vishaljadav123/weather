//
//  Constant.swift
//  Weather Demo
//
//  Created by Vishal Jadav on 25/05/22.
//

import Foundation
import UIKit

public struct Constant{
    
    enum AppKeyAndUrls :String {
        case baseURL    = "http://api.weatherstack.com/current"
        case appKey     = "0ea6770bd9e8c5fa2f2b1c5305211692"
    }
    
}
