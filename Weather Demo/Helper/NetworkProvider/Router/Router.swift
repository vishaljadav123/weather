//
//  Router.swift
//  Weather Demo
//
//  Created by Vishal Jadav on 25/05/22.
//

import Foundation
import Alamofire
import LanguageManager_iOS

// MARK: - URLRequestConvertible

/**
 Types adopting the `URLRequestConvertible` protocol can be used to construct URL requests.
 */
public protocol URLRequestConvertible {
    /// The URL request.
    var urlRequest: URLRequest { get }
}

enum Router: URLRequestConvertible {
    
    static let baseUrl = Constant.AppKeyAndUrls.baseURL
    
    case getWeatherData(info:String)
    
    //Request Type
    var method: String {
        switch self {
            
        case .getWeatherData:
            return Alamofire.HTTPMethod.get.rawValue
        }
    }
    
   // "?access_key=\(Constant.AppKeyAndUrls.appKey.rawValue)&query=\(info)&language=\(LanguageManager.shared.currentLanguage)"
    var path: String {
        switch self {
        case .getWeatherData(let info):
            return "?access_key=\(Constant.AppKeyAndUrls.appKey.rawValue)&query=\(info)"
        }
    }
    
    //Generate request
    public var urlRequest:URLRequest {
        
        let completeURLString = Router.baseUrl.rawValue + path
        let URLSTR = completeURLString.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
        let url = URL(string: URLSTR!)
        var request = URLRequest(url: url!)
        request.httpMethod = method
        
        do{
            return try Alamofire.JSONEncoding.default.encode(request)
        }
        catch
        {
            print(error.localizedDescription)
        }
        return request
    }
}
