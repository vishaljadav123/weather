//
//  WeatherDataOperator.swift
//  Weather Demo
//
//  Created by Vishal Jadav on 25/05/22.
//

import Foundation

protocol Operator {
    associatedtype P = NetworkProvider
    
    func getRoute() -> Router
}

class WeatherDataOperator: Operator {
    
    var locationInfo: String
    
    init(info: String) {
        self.locationInfo = info
    }
    
    public func getRoute() -> Router {
        return Router.getWeatherData(info:self.locationInfo)
    }
}
