//
//  LocationManager.swift
//  Weather Demo
//
//  Created by vishal jadav on 25/05/22.
//

import Foundation
import CoreLocation
import UIKit

class CurrentLocationTrackManager: NSObject,CLLocationManagerDelegate {
    
    var locationManager: CLLocationManager?
    private static var privateShared : CurrentLocationTrackManager?
    typealias completionHandler = (_ location: CLLocation?,_ error: String?) -> Void
    var completion: completionHandler?
    
    class func shared() -> CurrentLocationTrackManager {
        guard let uwShared = privateShared else {
            privateShared = CurrentLocationTrackManager()
            return privateShared!
        }
        return uwShared
    }
    
    class func destroy() {
        privateShared = nil
    }
    
    func stopLocationManager() {
        locationManager?.stopUpdatingLocation()
    }
    
    func startMonitoringLocationWithBlock(handler:@escaping (_ location: CLLocation?,_ error: String?) -> Void){
        restartMonitoringLocation()
        self.completion = handler
        if locationManager == nil{
            locationManager = CLLocationManager()
            locationManager?.delegate = self
            locationManager?.desiredAccuracy = kCLLocationAccuracyBest
            locationManager?.requestAlwaysAuthorization()
        }
        locationManager?.stopUpdatingLocation()
        locationManager?.startUpdatingLocation()
    }
    
    func restartMonitoringLocation() {
        locationManager?.stopUpdatingLocation()
        if CLLocationManager.authorizationStatus() == .authorizedAlways ||  CLLocationManager.authorizationStatus() == .authorizedWhenInUse{
            locationManager?.startUpdatingLocation()
        } else if CLLocationManager.authorizationStatus() == .denied{
            print("Location service is disable")
            let alertController = UIAlertController(title: "Location Permission Required", message: "Please enable location permissions in settings.", preferredStyle: UIAlertController.Style.alert)
            
            let okAction = UIAlertAction(title: "Settings", style: .default, handler: {(cAlertAction) in
                //Redirect to Settings app
                UIApplication.shared.open(URL(string:UIApplication.openSettingsURLString)!)
            })
            
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel)
            alertController.addAction(cancelAction)
            
            alertController.addAction(okAction)
            
            UIApplication.shared.windows.first?.rootViewController?.present(alertController, animated: true, completion: nil)
        }else{
            locationManager?.requestAlwaysAuthorization()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]){
        let location = locations.last
        if let handler = completion {
            handler(location, nil)
        }
        stopLocationManager()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        if let handler = completion {
            handler(nil, error.localizedDescription)
        }
    }
    
    func getCityName(location:CLLocation ,city: @escaping (String) -> ())  {
        let geocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(location) { (placemarksArray, error) in
            if (placemarksArray?.count ?? 0) > 0 {
                if let placemark = placemarksArray?.first , let getCity = placemark.locality {
                    city("\(getCity)")
                }
            }
        }
    }
    
}
