//
//  ViewController.swift
//  Weather Demo
//
//  Created by vishal jadav on 24/05/22.
//

import UIKit
import LanguageManager_iOS

class MainViewController: UIViewController {
    
    /// Outlet Collection
    
    @IBOutlet var lblTemp: UILabel!
    @IBOutlet var lblType: UILabel!
    @IBOutlet var lblWindSpeed: UILabel!
    @IBOutlet var lblHumidity: UILabel!
    @IBOutlet var lblLocatiion: UILabel!
    
    /// Variables Declrations
    
    private let viewModel = WeatherViewModel()
    
    /// Fucntions Declrations
    
    func applyStyle() {
        UIApplication.shared.statusBarView?.backgroundColor = UIColor.init(named: "ColorBlue")
        LanguageManager.shared.defaultLanguage = .en
        self.getTempInfo()
    }
    
    func checkInternet() {
        guard Reachability.isConnectedToNetwork() else {
            let alert = UIAlertController(title: "Weather Info", message: "Please turn on your internet to get updated weather info.", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return
        }
    }
    
    func getTempInfo() {
        
        self.checkInternet()
        
        SwiftLoader.show(animated: true)
        
        CurrentLocationTrackManager.shared().startMonitoringLocationWithBlock { location, error in
            guard error == nil  else {
                SwiftLoader.hide()
                return
            }
            if let getLocationInfo =  location {
                CurrentLocationTrackManager.shared().getCityName(location: getLocationInfo) { cityName in
                    self.viewModel.getWeatherDataForLocation(location: cityName) { result in
                        DispatchQueue.main.async {
                            print(result)
                            SwiftLoader.hide()
                            self.lblLocatiion.text = result.location.localiz()
                            self.lblTemp.text = result.temp.localiz()
                            self.lblWindSpeed.text = result.wind.localiz()
                            self.lblType.text = result.type.localiz()
                            self.lblHumidity.text = result.humidity.localiz()
                        }
                    }
                }
            }
        }
    }
    
    /// IBActions Declrations

    @IBAction func changeLangTapped(_ sender: UIBarButtonItem) {
        let alert = UIAlertController(title: nil, message: "Change Language", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "English", style: .default , handler:{ (UIAlertAction)in
            self.checkInternet()
            self.viewModel.setLang(isEng: true)
        }))
        
        alert.addAction(UIAlertAction(title: "Hindi".localiz(), style: .default , handler:{ (UIAlertAction)in
            self.checkInternet()
            self.viewModel.setLang(isEng: false)
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:{ (UIAlertAction)in
            
        }))
        
        alert.popoverPresentationController?.sourceView = self.view
        
        self.present(alert, animated: true, completion: {
        })
        
    }
    
    //------------------------------------------------------
    //MARK: Life Cycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.applyStyle()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }

    //------------------------------------------------------
    //MARK:- Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        print("💥💥 deinit \(type(of: self)) 💥💥")
    }
    
}

