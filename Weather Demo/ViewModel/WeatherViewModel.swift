//
//  WeatherViewModel.swift
//  Weather Demo
//
//  Created by Vishal Jadav on 25/05/22.
//

import Foundation

import UIKit
import LanguageManager_iOS

typealias completionFetchCityWeather =  (_ result:WeatherInfo) -> Void

class WeatherViewModel: NSObject,NetworkProvider {
    
    let mapper          =   WeatherDataMapper()
    
    func getWeatherDataForLocation(location: String, completionHandler: @escaping completionFetchCityWeather){
        
        let dataOperator = WeatherDataOperator(info: location)
        
        self.request(router: dataOperator.getRoute()) { (parsedData) in
            
            print(parsedData)
            
            self.mapper.map(jsonData: parsedData, completionHandler: { weatherInfo in
                
                completionHandler(weatherInfo)
                
            })
        }
    }
    
    func setLang(isEng:Bool) {
        LanguageManager.shared.setLanguage(language: isEng ? .en : .hi)
        { title -> UIViewController in
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            return storyboard.instantiateInitialViewController()!
        } animation: { view in
            view.transform = CGAffineTransform(scaleX: 1, y: 1)
            view.alpha = 0
        }
    }
    
}
