//
//  AppDelagate+Extension.swift
//  Weather Demo
//
//  Created by vishal jadav on 25/05/22.
//

import Foundation
import UIKit

extension UIApplication {
    
    var statusBarView: UIView? {
        
        let statusBar =  UIView()
        statusBar.frame =  (UIApplication.shared.windows.first?.windowScene?.statusBarManager!.statusBarFrame)!
        self.windows.first?.addSubview(statusBar)
        return statusBar
        
    }
    
}
