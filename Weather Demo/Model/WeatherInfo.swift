//
//  WeatherInfo.swift
//  Weather Demo
//
//  Created by Vishal Jadav on 25/05/22.
//

import Foundation

class WeatherInfo: NSObject {
    
    var location = ""
    var temp = ""
    var type  = ""
    var wind  = ""
    var humidity = ""
    
    init(location:String, temp:String, type:String, wind:String, humidity:String) {
        self.location  = location
        self.temp      = temp
        self.type      = type
        self.wind      = wind
        self.humidity  = humidity
    }
    
}
