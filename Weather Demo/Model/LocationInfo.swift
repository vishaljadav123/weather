//
//  LocationInfo.swift
//  Weather Demo
//
//  Created by Vishal Jadav on 25/05/22.
//

import Foundation
class LocationInfo: NSObject {
    
    var lat:String   =   ""
    var lng:String   =   ""
    
    init(lat:String,lng:String) {
        self.lat     =       lat
        self.lng     =       lng
    }
}
