//
//  WeatherDataMapper.swift
//  Weather Demo
//
//  Created by Vishal Jadav on 25/05/22.
//

import Foundation
import LanguageManager_iOS

protocol Mapper {
    func map(jsonData: AnyObject, completionHandler: @escaping (_ success:WeatherInfo) -> Void)
}

class WeatherDataMapper: Mapper {
    
    func map(jsonData: AnyObject, completionHandler: @escaping (_ success:WeatherInfo) -> Void){
        
        guard let response = jsonData as? [String:Any] else { return }
        
        guard let currentInfo = response["current"] as? [String:Any] else { return }
        guard let locationInfo = response["location"]  as? [String:Any] else { return }
        
        let getLocation = "\(locationInfo["name"] as? String ?? ""), \(locationInfo["country"] as? String ?? "")"
        let getTemp = "\(currentInfo["temperature"] as? Int ?? 0) °C"
        guard let getType = currentInfo["weather_descriptions"] as? [String] else { return }
        let type = "\(getType.first ?? "")"
        let wind = "\(currentInfo["wind_speed"] as? Int ?? 0) km/h"
        let humidity = "\(currentInfo["humidity"] as? Int ?? 0) %"
        
        
        let weatherInfoModel = WeatherInfo.init(location: getLocation, temp: getTemp, type: type, wind: wind, humidity: humidity)
        completionHandler(weatherInfoModel)
        
        
    }
}
